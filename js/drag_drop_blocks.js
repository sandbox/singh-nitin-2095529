/**
 * @file
 * Attaches drag drop and resize behavior for the drag drop blocks module.
 */

var parent = children = '';
(function($) {
  Drupal.behaviors.DragDropBlocksModule = {
    attach: function(context, settings) {
      $('li > em').click(function() {
        if ($(this).hasClass('close')) {
          $(this).parent().find('div.drag-drop-blocks-handle').hide()
          $(this).attr('class', 'plus');
          $(this).html('+');
        } else {
          $(this).parent().find('div.drag-drop-blocks-handle').show()
          $(this).attr('class', 'close');
          $(this).html('x');
        }
      });
      $('#sortable > li.li-parent').resizable({
        stop: function(e, ui) {
          saveOrder();
        }
      });
      $("#sortable").sortable({items: '> li > div',
        placeholder: 'drag-drop-blocks-placeholder',
        update: function(e, ui) {
          saveOrder();
        },
        start: function(e, ui) {
          parent = $(ui.item.context.parentElement).attr('id');
          children = $(ui.item.context.parentElement).find('div.drag-drop-blocks-handle').attr('id');
        },
        beforeStop: function(e, ui) {
          placeholder = $(ui.placeholder).attr('class')
          $(ui.item.context.parentElement).find('div.drag-drop-blocks-handle').each(function() {
            if ($(this).hasClass(placeholder)) {
              //Nothing to do
            } else {
              id = $(this).attr('id')
              if (id != children) {
                $('#' + parent).append($('#' + id))
              }
            }
          });
        }
      });
    }
  };
  saveOrder = function() {
    var order = [];
    $('#sortable li.li-parent').each(function() {
      var params = {}
      params['bid'] = $(this).find('div.drag-drop-blocks-handle').attr('bid');
      params['width'] = $(this).width();
      params['height'] = $(this).height();
      order.push(params);
    });
    $.ajax({
      type: "POST",
      data: {'order': order, 'page': $('#sortable').attr('page')},
      url: '?q=admin/drag_drop_blocks/save/order',
      success: function(resp) {
        console.log(resp);
      }
    });
  };
})(jQuery);
