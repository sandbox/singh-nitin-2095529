-------------------------------------------------------------------------------
                            Drag Drop Blocks
-------------------------------------------------------------------------------

Administrator of the site can create a page, and select blocks which will show
on that page. After that admin can drag drop of any block in the page to change
the position of block and also change the width and height of each and every 
block. Administrator can set default width and height of every block.


Intallation
-----------

Copy drag_drop_blocks.module to your module directory and then enable on the 
admin modules page.

Author
------
Nitin Kumar Singh
nitin@incaendo.com
