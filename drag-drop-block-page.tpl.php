<?php

/**
 * @file
 * Default theme implementation to format all blocks to display
 * on a page.
 *
 * Available variables:
 * - $blocks: Having the html of blocks.
 */
?>
<ul id="sortable" page="<?php echo $q; ?>">
  <?php echo $blocks; ?>
</ul>
