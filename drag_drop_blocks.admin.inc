<?php

/**
 * @file
 * Admin page callbacks for the drag_drop_blocks module.
 */

/**
 * Callback function to render the list of pages.
 * 
 * @return string
 *   HTML of the page
 */
function drag_drop_blocks_admin_list() {
  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'title'),
    'page' => array('data' => t('URL'), 'field' => 'page'),
    'access' => array('data' => t('Access Callback')),
    'operations' => array('data' => t('Operations')),
  );
  $query = db_select('drag_drop_blocks', 'd')->extend('PagerDefault')->extend('TableSort');
  $result = $query
    ->fields('d', array('id', 'page', 'title', 'access'))
    ->limit(50)
    ->orderByHeader($header)
    ->execute();
  $rows = array();
  foreach ($result as $row) {
    $rows[] = array(
      'title' => l($row->title, $row->page),
      'page' => l($row->page, $row->page),
      'access' => $row->access,
      'operations' => l(t('Edit'), 'admin/structure/blocks/drag_drop/' . $row->id . '/edit') . ' | ' . l(t('Delete'), 'admin/structure/blocks/drag_drop/' . $row->id . '/delete'));
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Function to render add new form for page and configure blocks for that page.
 * 
 * @param int $id
 *   is the id of an entry in the DB to get the info
 * 
 * @return string
 *   HTML of the page
 */
function drag_drop_blocks_admin($id = 0) {
  return drupal_get_form('drag_drop_blocks_admin_overview', $id);
}

/**
 * Form builder for the drag drop blocks administration form.
 *
 * @ingroup forms
 * @see drag_drop_admin_overview_validate()
 * @see drag_drop_admin_overview_submit()
 */
function drag_drop_blocks_admin_overview($form, &$form_state, $id = 0) {
  global $theme_key;
  $existing_info = $setting_default = array();
  if ($id) {
    $existing_info = drag_drop_blocks_load_info(array('id' => $id));
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $existing_info['id'],
    );
    $form['#widget_order'] = unserialize($existing_info['widget_order']);
    $settings_saved = unserialize($existing_info['settings']);
    if (!empty($settings_saved)) {
      foreach ($settings_saved as $key => $setting) {
        if (isset($setting['bid'])) {
          $setting_default[$key] = $setting['bid'];
        }
        else {
          $setting_default[$key] = 0;
        }
      }
    }
  }
  $form['page'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => isset($existing_info['page']) ? $existing_info['page'] : '',
    '#required' => TRUE,
  );
  $form['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($existing_info['title']) ? $existing_info['title'] : '',
    '#required' => TRUE,
    '#attributes' => array('maxlength' => 60),
  );
  $form['page_access'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Callback'),
    '#default_value' => isset($existing_info['access']) ? $existing_info['access'] : '',
    '#required' => FALSE,
    '#description' => t('Leave blank, if all users have the permission to view this page.'),
    '#attributes' => array('maxlength' => 60),
  );
  // Fetch and sort blocks.
  $blocks = _block_rehash($theme_key);
  $options = array();
  foreach ($blocks as $block) {
    $options[$block['bid']] = $block['info'];
  }
  $form['settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Blocks'),
    '#default_value' => $setting_default,
    '#options' => $options,
    '#description' => t('Blocks you want to show on the above page'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#blocks'] = $blocks;
  return $form;
}


/**
 * Executes validation.
 *
 * @see drag_drop_admin_overview()
 */
function drag_drop_blocks_admin_overview_validate($form, &$form_state) {

}

/**
 * Executes submit.
 *
 * @see drag_drop_admin_overview()
 */
function drag_drop_blocks_admin_overview_submit($form, &$form_state) {
  $blocks = $form['#blocks'];
  $settings_to_save = array();
  foreach ($form_state['values']['settings'] as $settings) {
    if ($settings) {
      foreach ($blocks as $block) {
        if ($block['bid'] == $settings) {
          $settings_to_save[$settings] = $block;
        }
      }
    }
    else {
      $settings_to_save[$settings] = array();
    }
  }
  if (isset($form_state['values']['id'])) {
    $widget_orders = isset($form['#widget_order']) && is_array($form['#widget_order']) ? $form['#widget_order'] : array();
    $order_keys = array_keys($settings_to_save);
    $widget_keys = array_keys($widget_orders);
    $new_widget_to_add = array_diff($order_keys, $widget_keys);
    $old_widget_to_remove = array_diff($widget_keys, $order_keys);
    if (!empty($new_widget_to_add)) {
      foreach ($new_widget_to_add as $bid) {
        $widget_orders[$bid] = array(
          'bid' => $bid,
          'width' => 0,
          'height' => 0,
        );
      }
    }
    if (!empty($old_widget_to_remove)) {
      foreach ($old_widget_to_remove as $bid) {
        unset($widget_orders[$bid]);
      }
    }
    db_update('drag_drop_blocks')
    ->fields(array(
      'page' => $form_state['values']['page'],
      'settings' => serialize($settings_to_save),
      'widget_order' => serialize($widget_orders),
      'title' => $form_state['values']['page_title'],
      'access' => $form_state['values']['page_access'],
    ))
    ->condition('id', $form_state['values']['id'])
    ->execute();
    drupal_set_message(t('Page updated successfully.'));
  }
  else {
    $query = db_insert('drag_drop_blocks')->fields(array('page', 'settings'));
    $query->values(array(
            'page' => $form_state['values']['page'],
            'settings' => serialize($settings_to_save),
            'title' => $form_state['values']['page_title'],
            'access' => $form_state['values']['page_access'],
          ));
    $query->execute();
    drupal_set_message(t('Page added successfully.'));
  }
  menu_rebuild();
  $form_state['redirect'] = 'admin/structure/blocks/drag_drop';
}

/**
 * Ajax Callback to save the order, width and height of a particular page.
 */
function drag_drop_blocks_admin_save_order() {
  if (!empty($_POST)) {
    $post_data = $_POST;
    $orders = $post_data['order'];
    $order_to_save = array();
    foreach ($orders as $order) {
      $order_to_save[$order['bid']] = $order;
    }
    if (!empty($order_to_save)) {
      db_update('drag_drop_blocks')
      ->fields(array(
        'widget_order' => serialize($order_to_save),
      ))
      ->condition('page', $post_data['page'])
      ->execute();
    }
  }
}

/**
 * Administrator settings form.
 */
function drag_drop_blocks_admin_settings($form, &$form_state) {
  $form['drag_drop_blocks_default_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('drag_drop_blocks_default_width', 300),
    '#description' => t('Default height of every block in pixels only.'),
  );
  $form['drag_drop_blocks_default_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('drag_drop_blocks_default_height', 'auto'),
    '#description' => t('Default height of every block in pixels, you can set auto as well.'),
  );
  return system_settings_form($form);
}

/**
 * Form constructor for the page deletion confirmation form.
 *
 * @see drag_drop_blocks_delete_confirm_submit()
 */
function drag_drop_blocks_delete_confirm($form, &$form_state, $id) {
  // Always provide entity id in the same form key as in the entity edit form.
  $info = drag_drop_blocks_load_info(array('id' => $id));
  $form['#info'] = $info;
  $form['drag_drop_block_id'] = array('#type' => 'value', '#value' => $id);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $info['page'])),
    'admin/structure/blocks/drag_drop',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes deletion.
 *
 * @see drag_drop_blocks_delete_confirm()
 */
function drag_drop_blocks_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $info = $form['#info'];
    db_delete('drag_drop_blocks')
      ->condition('id', $info['id'])
      ->execute();
    drupal_set_message(t('%title has been deleted.', array('%title' => $info['page'])));
  }
  menu_rebuild();
  $form_state['redirect'] = 'admin/structure/blocks/drag_drop';
}
