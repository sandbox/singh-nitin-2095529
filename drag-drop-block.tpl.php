<?php

/**
 * @file
 * Default theme implementation to format an individual block for display
 * on a page.
 *
 * Available variables:
 * - $info: All the information for the block.
 * - $block: Having subject and content of the block.
 */
?>
<?php
  $style = '';
  if ($info['width']):
    $style .= "width: " . $info['width'] . "px; ";
  endif;
  if ($info['height']):
    if (is_numeric($info['height'])):
      $style .= "height: " . $info['height'] . "px; ";
    else:
      $style .= "height: " . $info['height'];
    endif;
  endif;
?>
<li class="li-parent" id="drag-drop-blocks-<?php echo $info['bid']; ?>" style="<?php echo $style; ?>">
  <!--<em class="close">x</em>-->
  <div id="drag-drop-blocks-<?php echo $info['bid']; ?>-div" class="drag-drop-blocks-handle" bid="<?php echo $info['bid']; ?>">
    <div id="block-<?php echo $info['module']; ?>-<?php echo $info['delta']; ?>">
    <?php
      print render($block['subject']);
      print render($block['content']);
    ?>
  </div>
</li>
