<?php

/**
 * @file
 * User page callbacks for the drag_drop_blocks module.
 */

/**
 * Callback function to render the page.
 * 
 * @return string
 *   Html of the page
 */
function drag_drop_blocks_page() {
  $q = $_GET['q'];
  $page_info = drag_drop_blocks_load_info(array('page' => $q));
  if (empty($page_info)) {
    drupal_not_found();
  }
  else {
    $output = '';
    drupal_add_library('system', 'ui.resizable');
    drupal_add_library('system', 'ui.sortable');
    $blocks = unserialize($page_info['settings']);
    $block_orders = unserialize($page_info['widget_order']);
    $admin = user_access('administer drag drop blocks');
    if (!empty($block_orders)) {
      foreach ($block_orders as $bid => $order) {
        if (isset($blocks[$bid]) && !empty($blocks[$bid])) {
          $info = $blocks[$bid];
          $info['width'] = isset($order['width']) && $order['width'] ? $order['width'] : variable_get('drag_drop_blocks_default_width', 300);
          $info['height'] = isset($order['width']) && $order['height'] ? $order['height'] : variable_get('drag_drop_blocks_default_height', 'auto');
          $block = module_invoke($info['module'], 'block_view', $info['delta']);
          if ($admin) {
            if (empty($block)) {
              $block = array('subject' => $info['title'], 'content' => t('No content available.'));
            }
          }
          if (!empty($block)) {
            $output .= theme('drag_drop_block', array('block' => $block, 'info' => $info));
          }
        }
      }
    }
    else {
      foreach ($blocks as $info) {
        if (!empty($info)) {
          $info['width'] = variable_get('drag_drop_blocks_default_width', 300);
          $info['height'] = variable_get('drag_drop_blocks_default_height', 'auto');
          $block = module_invoke($info['module'], 'block_view', $info['delta']);
          $output .= theme('drag_drop_block', array('block' => $block, 'info' => $info));
        }
      }
    }
    if ($output) {
      $output = theme('drag_drop_block_page', array('blocks' => $output, 'q' => $q));
    }
    return $output;
  }
}
